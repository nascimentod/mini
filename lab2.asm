;glowna petla programu
;funkcja łopatologicznie sprawdza każdy z przycisków z osobna 
;0FE00H -> adres bufora klawiatury
;P1.6 i p1.7 -> odpowiednio pierwszy i drugi wiersz przycisków
;R7 -> rejestr, do którego musimy zapisać kod ascii znaku, który ma się wypisać na wyświetlaczu
;DISP ->funkcja juz zaimplementowana, odpowiedzialna za wyswietlanie znakow
;#30H -> kod ascii '0'
;#41H -> kod ascii 'A'
;trochę rozjechały się indentacje, ale to nie Python, więc pół biedy :P

PETLA:  NOP

START:
        MOV DPTR, #OFE00H
        SETB P1.7
		CLR P1.6 ; ustawiamy pierwszy wiersz na 0 i szukamy zer w buforze. jesli znajdziemy - przycisk ten jest wcisniety
		MOVX A, @DPTR
		CJNE A, #0FFH, DALEJ
		SJMP DRUGI_WIERSZ
	DALEJ:
	       MOV B, A
		   JB B.0, E1
		   MOV R7, #30H
		   ACALL DISP
		   SJMP START
	E1:
	       JB B.1, E2
		   MOV R7, #31H
		   ACALL DISP
		   SJMP START
    E2:
	       JB B.2, E3
		   MOV R7, #32H
		   ACALL DISP
		   SJMP START
    E3:
	       JB B.3, E4
		   MOV R7, #33H
		   ACALL DISP
		   SJMP START
    E4:
	       JB B.4, E5
		   MOV R7, #34H
		   ACALL DISP
		   SJMP START
	E5:
	       JB B.5, E6
		   MOV R7, #35H
		   ACALL DISP
		   SJMP START
	E6:
	       JB B.6, E7
		   MOV R7, #36H
		   ACALL DISP
		   SJMP START
	E7:
	       JB B.7, DRUGI_WIERSZ
		   MOV R7, #37H
		   ACALL DISP
		   SJMP START
    DRUGI_WIERSZ: 
	       SETB P1.6
		   CLR P1.7
		   MOVX A, @DPTR
		   CJNE A, #0FFH, DALEJ2
		   SJMP START
	DALEJ2:
	       MOV B, A
		   JB B.0, F1
		   MOV R7, #38H
		   ACALL DISP
		   SJMP START
	F1:
	       JB B.1, F2
		   MOV R7, #39H
		   ACALL DISP
		   JMP START
	F2:
	       JB B.2, F3
		   MOV R7, #41H
		   ACALL DISP
		   JMP START
	F3:
	       JB B.3, F4
		   MOV R7, #42H
		   ACALL DISP
		   JMP START
	F4:
	       JB B.4, F5
		   MOV R7, #43H
		   ACALL DISP
		   JMP START
	F5:
	       JB B.5, F6
		   MOV R7, #44H
		   ACALL DISP
		   JMP START
	F6:
          JB B.6, F7
		   MOV R7, #45H
		   ACALL DISP
		   JMP START
	F7:
	       JB B.7, BACK
		   MOV R7, #46H
		   ACALL DISP
		   JMP START
		   
	BACK: JMP START
	
	JMP PETLA
